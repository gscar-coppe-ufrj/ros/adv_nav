// spatial_oem_nodelet.h



#ifndef SPATIAL_OEM_SPATIAL_OEM_SPACIAL_OEM_NODELET_H
#define SPATIAL_OEM_SPATIAL_OEM_SPACIAL_OEM_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/Float32.h>
#include <adv_nav/RPY.h>
#include <adv_nav/spatial_oem/spatial_oem.h>



#define SPACIAL_OEM_RADIANS_TO_DEGREES (180.0/M_PI)



namespace adv_nav {



class SpatialOEMNodelet : public SpatialOEM, public nodelet::Nodelet
{
    ros::NodeHandle nodeHandle;

    //Messages
    sensor_msgs::Imu msgIMU;
    sensor_msgs::NavSatFix msgNavSat;
    geometry_msgs::TwistStamped msgVelocity;
    geometry_msgs::Vector3Stamped msgMagnetic;
    std_msgs::Float32 msgTemperature;
    RPY msgRPY;

    ros::Publisher msgIMUPub;
    ros::Publisher msgFixPub;
    ros::Publisher msgFixExtPub;
    ros::Publisher msgVelocityPub;
    ros::Publisher msgMagneticPub;
    ros::Publisher msgTemperaturePub;
    ros::Publisher msgRPYPub;

    void SendData(an_decoder_t* anDecoder);

public:
    SpatialOEMNodelet();
    virtual void onInit();
};



}


#endif
