// spatial_oem.h



#ifndef SPATIAL_OEM_SPATIAL_OEM_H
#define SPATIAL_OEM_SPATIAL_OEM_H



#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>
#include <boost/thread.hpp>
#include <linux_devs/linux_dev_manager.h>
#include <adv_nav/adv_nav_pac_prot_lib/rs232/rs232.h>
#include <adv_nav/adv_nav_pac_prot_lib/an_packet_protocol.h>
#include <adv_nav/adv_nav_pac_prot_lib/spatial_packets.h>



namespace adv_nav {



class SpatialOEM : public linux_devs::LinuxDevManager
{
protected:
    bool deviceOnOff;
    bool dataOnOff;
    float angles[3];
    float angularAcceleration[3];
    float linearAcceleration[3];

    boost::thread* thread;
    boost::mutex m;
    volatile bool running;
    bool connected;
    bool rawMode;

    std::string port;
    int baudRate;

    packet_periods_packet_t packets2request;

    static void Thread(SpatialOEM* object);
    static int AnPacketTransmit(an_packet_t* anPacket);
    void Loop();
    virtual void SendData(an_decoder_t* anDecoder) = 0;

    void ChangeSettings(std::string p, int baud);
    bool OpenPort();

    virtual void SelectDevices(const std::vector<std::string> & list);

public:
    SpatialOEM();
    ~SpatialOEM();
};



}



#endif
