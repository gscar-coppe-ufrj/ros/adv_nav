// spatial_oem.cpp



#include <adv_nav/spatial_oem/spatial_oem.h>



adv_nav::SpatialOEM::SpatialOEM()
{
    deviceOnOff = false;
    dataOnOff = false;

    baudRate = 115200;

    rawMode = true;

    connected = false;

    packets2request.permanent = 0;
    packets2request.clear_existing_packets = 1;
    for (auto i = 0; i < MAXIMUM_PACKET_PERIODS; i++)
    {
        packets2request.packet_periods[i].packet_id = 0;
        packets2request.packet_periods[i].period = 0;
    }

    uint8_t ids[] = {packet_id_quaternion_orientation, packet_id_euler_orientation_standard_deviation, packet_id_raw_sensors, packet_id_system_state, packet_id_body_velocity};
    uint32_t per[] = {100, 100, 100, 100, 100}; // Period for each packet
    for (int k = 0; k < 5; k++)
    {
        packet_period_t packPer;
        packPer.packet_id = ids[k];
        packPer.period = per[k];
        packets2request.packet_periods[k] = packPer;
    }
}


void adv_nav::SpatialOEM::ChangeSettings(std::string p, int baud)
{
    running = false;
    thread->join();
    port = p;
    baudRate = baud;
    delete thread;
    thread = new boost::thread(SpatialOEM::Thread, this);
}


int adv_nav::SpatialOEM::AnPacketTransmit(an_packet_t* anPacket)
{
    an_packet_encode(anPacket);
    return SendBuf(an_packet_pointer(anPacket), an_packet_size(anPacket));
}


void adv_nav::SpatialOEM::Thread(SpatialOEM* object)
{
    object->Loop();
}


void adv_nav::SpatialOEM::Loop()
{
    an_decoder_t anDecoder;
    an_decoder_initialise(&anDecoder);

    int bytesReceived;

    while (running)
    {
        m.lock();
        if (connected)
        {
            if ((bytesReceived = PollComport(an_decoder_pointer(&anDecoder), an_decoder_size(&anDecoder))) > 0)
            {
                /* increment the decode buffer length by the number of bytes received */
                an_decoder_increment(&anDecoder, bytesReceived);

                /* decode all the packets in the buffer */
                SendData(&anDecoder);
            }
            m.unlock();
            usleep(10000);
        }
        else
        {
            if (!OpenPort())
            {
                m.unlock();
                usleep(10000000);
            }
            else
                m.unlock();
        }
    }
}


bool adv_nav::SpatialOEM::OpenPort()
{
    if (OpenComport((char*)port.c_str(), baudRate))
    {
        std::cout << "Could not Open IMU" << std::endl;
        return false;
    }
    else
    {
        connected = true;
        AnPacketTransmit(encode_packet_periods_packet(&packets2request));
        return true;
    }
}


void adv_nav::SpatialOEM::SelectDevices(const std::vector<std::string> & list)
{
    bool close = false;

    if (list.size() > 0)
    {
        auto temp = list.front();

        std::string str = "tty";
        auto i = temp.find(str);
        if (i != std::string::npos)
        {
            port = temp;
            close = true;
        }
    }
    else
        close = true;

    if (close)
    {
        m.lock();
        if (connected)
        {
            CloseComport();
            port = "";
            connected = false;
        }
        m.unlock();
    }
}


adv_nav::SpatialOEM::~SpatialOEM()
{
    running = false;
    thread->join();
}
