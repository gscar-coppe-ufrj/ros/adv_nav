// spatial_oem_nodelet.cpp



#include <adv_nav/spatial_oem/spatial_oem_nodelet.h>



adv_nav::SpatialOEMNodelet::SpatialOEMNodelet()
{
    // Values from datasheet (errors squared)
    double varVel = (0.05);
    msgIMU.angular_velocity_covariance[0] = std::pow(varVel,2);
    msgIMU.angular_velocity_covariance[4] = std::pow(varVel,2);
    msgIMU.angular_velocity_covariance[8] = std::pow(varVel,2);

    double varAcel = (-1); // IMU doesn't supply acel covariance
    msgIMU.linear_acceleration_covariance[0] = varAcel;
}


void adv_nav::SpatialOEMNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    privateNH.param("port", port, std::string(""));
    privateNH.param("baud_rate", baudRate, baudRate);
    privateNH.param("config", configPath, std::string(""));
    privateNH.param("field_name", fieldName, std::string(""));

    nodeHandle = getNodeHandle();

    msgIMUPub = nodeHandle.advertise<sensor_msgs::Imu>(getName() + "/data", 1000);
    msgFixPub = nodeHandle.advertise<sensor_msgs::NavSatFix >(getName() + "/fix", 1000);
    msgVelocityPub= nodeHandle.advertise<geometry_msgs::TwistStamped>(getName() + "/velocity", 1000);
    msgMagneticPub = nodeHandle.advertise<geometry_msgs::Vector3Stamped>(getName() + "/magnetic", 1000);
    msgTemperaturePub = nodeHandle.advertise<std_msgs::Float32>(getName() + "/temperature", 1000);
    msgRPYPub = nodeHandle.advertise<RPY>(getName() + "/rpy", 1000);

    netLink = "udev";
    subSystem = "tty";
    std::vector<std::string> list;
    list.push_back(port);
    ConfigLinuxDev(getNodeHandle(), list, getName());

    running = true;
    thread = new boost::thread(SpatialOEMNodelet::Thread, this);
}


void adv_nav::SpatialOEMNodelet::SendData(an_decoder_t* anDecoder)
{
    an_packet_t* anPacket;

    //Packets to be requested
    system_state_packet_t systemStatePacket;
    raw_sensors_packet_t rawSensorsPacket;
    quaternion_orientation_packet_t quaternionPacket;
    euler_orientation_standard_deviation_packet_t eulerStdPacket;
    body_velocity_packet_t bodyVelocityPacket;

    while ((anPacket = an_packet_decode(anDecoder)) != NULL)
    {
        if (anPacket->id == packet_id_system_state)
        {
            if (decode_system_state_packet(&systemStatePacket, anPacket) == 0)
            {
                // Roll Pitch Yaw
                msgRPY.roll =systemStatePacket.orientation[0];
                msgRPY.pitch = systemStatePacket.orientation[1];
                msgRPY.yaw = systemStatePacket.orientation[2];
                msgRPYPub.publish(msgRPY);

                // Nav Sat
                msgNavSat.latitude = systemStatePacket.latitude;
                msgNavSat.longitude = systemStatePacket.longitude;
                msgNavSat.altitude = systemStatePacket.height;
                msgNavSat.position_covariance[0] = std::pow(systemStatePacket.standard_deviation[0], 2);
                msgNavSat.position_covariance[4] = std::pow(systemStatePacket.standard_deviation[1], 2);
                msgNavSat.position_covariance[8] = std::pow(systemStatePacket.standard_deviation[2], 2);
                msgFixPub.publish(msgNavSat);

                // Angular Velocity
                msgVelocity.twist.angular.x = systemStatePacket.angular_velocity[0];
                msgVelocity.twist.angular.y = systemStatePacket.angular_velocity[1];
                msgVelocity.twist.angular.z = systemStatePacket.angular_velocity[2];
                msgVelocityPub.publish(msgVelocity);

                // Non raw mode IMU
                if (!rawMode)
                {
                    msgIMU.angular_velocity.x = systemStatePacket.angular_velocity[0];
                    msgIMU.angular_velocity.y = systemStatePacket.angular_velocity[1];
                    msgIMU.angular_velocity.z = systemStatePacket.angular_velocity[2];
                    msgIMU.linear_acceleration.x  = systemStatePacket.body_acceleration[0];
                    msgIMU.linear_acceleration.y = systemStatePacket.body_acceleration[1];
                    msgIMU.linear_acceleration.z = systemStatePacket.body_acceleration[2];
                    msgIMUPub.publish(msgIMU);
                }
            }
        }
        else if (anPacket->id == packet_id_quaternion_orientation)
        {
            if (decode_quaternion_orientation_packet(&quaternionPacket, anPacket) == 0)
            {
                // Quaternion
                float * q = quaternionPacket.orientation;
                msgIMU.orientation.w = q[0];
                msgIMU.orientation.x = q[1];
                msgIMU.orientation.y = q[2];
                msgIMU.orientation.z = q[3];
                msgIMUPub.publish(msgIMU);
            }
        }
        else if (anPacket->id == packet_id_raw_sensors)
        {
            if(decode_raw_sensors_packet(&rawSensorsPacket, anPacket) == 0)
            {
                if ( rawMode )
                {
                    // Imu data (raw)
                    msgIMU.angular_velocity.x = rawSensorsPacket.gyroscopes[0];
                    msgIMU.angular_velocity.y = rawSensorsPacket.gyroscopes[1];
                    msgIMU.angular_velocity.z = rawSensorsPacket.gyroscopes[2];
                    msgIMU.linear_acceleration.x = rawSensorsPacket.accelerometers[0];
                    msgIMU.linear_acceleration.y = rawSensorsPacket.accelerometers[1];
                    msgIMU.linear_acceleration.z = rawSensorsPacket.accelerometers[2];
                    msgIMUPub.publish(msgIMU);
                }

                //Magnetic
                msgMagnetic.vector.x = rawSensorsPacket.magnetometers[0];
                msgMagnetic.vector.y = rawSensorsPacket.magnetometers[1];
                msgMagnetic.vector.z = rawSensorsPacket.magnetometers[2];
                msgMagneticPub.publish(msgMagnetic);

                //Temperature
                msgTemperature.data = rawSensorsPacket.imu_temperature;
                msgTemperaturePub.publish(msgTemperature);
            }
        }
        else if (anPacket->id == packet_id_euler_orientation_standard_deviation)
        {
            if (decode_euler_orientation_standard_deviation_packet(&eulerStdPacket, anPacket) == 0)
            {
                msgIMU.orientation_covariance[0] = std::pow(eulerStdPacket.standard_deviation[0], 2);
                msgIMU.orientation_covariance[4] = std::pow(eulerStdPacket.standard_deviation[1], 2);
                msgIMU.orientation_covariance[8] = std::pow(eulerStdPacket.standard_deviation[2], 2);
                msgIMUPub.publish(msgIMU);
            }
        }
        else if (anPacket->id == packet_id_body_velocity)
        {
            if (decode_body_velocity_packet(&bodyVelocityPacket, anPacket) == 0)
            {
                //Linear Velocity - outputs strange values!
                msgVelocity.twist.linear.x = bodyVelocityPacket.velocity[0];
                msgVelocity.twist.linear.y = bodyVelocityPacket.velocity[1];
                msgVelocity.twist.linear.z = bodyVelocityPacket.velocity[2];
                msgVelocityPub.publish(msgVelocity);
            }
        }
        /* Ensure that you free the an_packet when your done with it or you will leak memory */
        an_packet_free(&anPacket);
    }
}

